import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class hw7 {

    public static void main(String[] args) throws IOException {
        FileWriter fw = new FileWriter(new File("/Users/nissimlavy/Notes/OOP/hw7/dir_tree.txt"),
                true);
        File file = new File("/Users/nissimlavy/Notes/OOP");
        printDirTree(file, -1, fw);
    }

    public static void printDirTree(File f, int indentationLevel, FileWriter fw) throws IOException {
        if (!f.isDirectory()) {
            fw.write(padString(indentationLevel) + f.getName() + "\n");
            fw.flush();
            return;
        } else {
            fw.write(padString(indentationLevel) + f.getName() + "/" + "\n");
            fw.flush();
            File[] files = f.listFiles();
            for (File file : files)
                printDirTree(file, indentationLevel + 1, fw);
        }
    }

    public static String padString(int indentationLevel) {
        StringBuilder sb = new StringBuilder();

        int spaces = (indentationLevel * 4) - 2;
        if (spaces >= 0)
            for (int i = 0; i < spaces; i++) {
                sb.append(" ");
            }
        sb.append("|--");
        return sb.toString();
    }
}
