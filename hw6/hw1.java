import java.util.Scanner;
import java.util.StringTokenizer;

public class Hw1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Enter calculation: ");
        do {
            String input = s.nextLine();
            StringTokenizer st = new StringTokenizer(input);

            // check for incorrect input
            String value1 = "";
            if (st.hasMoreTokens()) {
                value1 = st.nextToken();
            }
            String operation = st.nextToken();

            // check for incorrect input
            String value2 = "";
            if (st.hasMoreTokens()) {
                value2 = st.nextToken();
            }

            // indicate that a number hasn't been initialized
            double dVal1 = Double.MIN_VALUE;
            double dVal2 = Double.MIN_VALUE;
            int iVal1 = Integer.MIN_VALUE;
            int iVal2 = Integer.MIN_VALUE;

            // input validation
            if (value1.matches("[A-Za-z]")) {
                System.out.println("Not a number!");
            } else if (!value1.matches(".*\\d.*")) {
                System.out.println("Missing number!");
            } else if (value1.contains(".")) {
                dVal1 = Double.parseDouble(value1);
            } else {
                iVal1 = Integer.parseInt(value1);
            }

            // input validation
            if (value2.matches("[A-Za-z]")) {
                System.out.println("Not a number!");
            } else if (!value2.matches(".*\\d.*")) {
                System.out.println("Missing number!");
            } else if (value2.contains(".")) {
                dVal2 = Double.parseDouble(value2);
            } else {
                iVal2 = Integer.parseInt(value2);
            }

            // execute proper operation
            if (operation.equals("+")) {
                if (dVal1 != Double.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(dVal1 + dVal2);
                } else if (dVal1 != Double.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(dVal1 + iVal2);
                } else if (iVal1 != Integer.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(iVal1 + dVal2);
                } else if (iVal1 != Integer.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(iVal1 + iVal2);
                }
            } else if (operation.equals("-")) {
                if (dVal1 != Double.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(dVal1 - dVal2);
                } else if (dVal1 != Double.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(dVal1 - iVal2);
                } else if (iVal1 != Integer.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(iVal1 - dVal2);
                } else if (iVal1 != Integer.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(iVal1 - iVal2);
                }
            } else if (operation.equals("*")) {
                if (dVal1 != Double.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(dVal1 * dVal2);
                } else if (dVal1 != Double.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(dVal1 * iVal2);
                } else if (iVal1 != Integer.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(iVal1 * dVal2);
                } else if (iVal1 != Integer.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(iVal1 * iVal2);
                }
            } else if (operation.equals("/")) {
                try {
                    if (dVal1 != Double.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                        System.out.println(dVal1 / dVal2);
                    } else if (dVal1 != Double.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                        System.out.println(dVal1 / iVal2);
                    } else if (iVal1 != Integer.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                        System.out.println(iVal1 / dVal2);
                    } else if (iVal1 != Integer.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                        System.out.println(iVal1 / iVal2);
                    }
                } catch (ArithmeticException ae) {
                    ae.printStackTrace();
                }
            } else if (operation.equals("%")) {
                if (dVal1 != Double.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(dVal1 % dVal2);
                } else if (dVal1 != Double.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(dVal1 % iVal2);
                } else if (iVal1 != Integer.MIN_VALUE && dVal2 != Double.MIN_VALUE) {
                    System.out.println(iVal1 % dVal2);
                } else if (iVal1 != Integer.MIN_VALUE && iVal2 != Integer.MIN_VALUE) {
                    System.out.println(iVal1 % iVal2);
                }
            } else {
                System.out
                        .println("IllegalOperationException: please re-enter with the correct function");
            }
            System.out.print("Enter calculation: ");
        } while (s.hasNext());
        s.close();
    }
}
