import java.util.Scanner;

public class Pyramid {
    public static void main(String[] args) throws Exception {
        Scanner in = new Scanner(System.in);

        int input = in.nextInt();

        for (int i = 1; i <= input; i++) {
            for (int j = 1; j <= input - i; j++)
                System.out.print("   ");
            for (int k = 1; k <= i; k++)
                if (k >= 10)
                    System.out.print(k);
                else
                    System.out.print("  " + (k));
            for (int k = i; k > 1; k--)
                if (k >= 10)
                    System.out.print(k);
                else
                    System.out.print("  " + (k - 1));
            System.out.println();

        }
    }
}
