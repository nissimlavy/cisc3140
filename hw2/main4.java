public class H2Q4 {

    public static void main(String[] args) {
        outerLoop: for (int i = 0; i < 5; i++) {
            innerLoop: for (int j = 5; j < 15; j++) {
                if (i > 2){
                    System.out.println("breaking outer loop");
                    break outerLoop;
                }
                if (j > 9){
                    System.out.println("breaking inner loop");
                    break innerLoop;
                }
            }
        }
    }
}
