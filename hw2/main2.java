import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Scanner;

public class Calendar1 {
    public static void main(String[] args) throws Exception {

        Scanner in = new Scanner(System.in);
        System.out.print("Enter year: ");
        int year = in.nextInt();
        for (int i = 1; i <= 12; i++) {
            int month = i;

            // bounds checking
            if (month < 1 || month > 12)
                throw new Exception("Invalid index for month: " + month);

            printCalendarHeader(month, year);

        }
        in.close();
    }

    private static void printCalendarHeader(int month, int year) {
        Calendar cal = new GregorianCalendar();
        cal.clear();
        cal.set(year, month - 1, 1);

        System.out.println("\n" + cal.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.US)
                + " " + cal.get(Calendar.YEAR));
        printCalendarTable(cal.getActualMaximum(Calendar.DAY_OF_MONTH),
                cal.get(Calendar.DAY_OF_WEEK));
    }

    private static void printCalendarTable(int numOfDaysInMoth, int startDayOfMonth) {

        int weekdayIndex = 0;

        System.out.println("Su  Mo  Tu  We  Th  Fr  Sa");

        for (int day = 1; day < startDayOfMonth; day++) {
            System.out.print("    "); //Add space for first day 
            weekdayIndex++;
        }

        // print table
        for (int day = 1; day <= numOfDaysInMoth; day++) {
            System.out.printf("%1$2d", day);
            weekdayIndex++;
            if (weekdayIndex == 7) { // reset spacing if it's the last day
                weekdayIndex = 0;
                System.out.println();
            } else {
                System.out.print("  ");
            }
        }

        System.out.println();
    }
}
