import java.util.Scanner;

public class H2Q3 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNext()) {
            String input = in.next();
            if (input.equals(new StringBuilder(input).reverse().toString())) {
                System.out.println("String is a palindrome");
            } else {
                System.out.println("String is not a palindrome");
            }
        }
    }

}
