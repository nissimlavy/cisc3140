import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class ShellEmulator {
    public static void main(String[] args) throws IOException, InterruptedException {
        Scanner s = new Scanner(System.in);
        FileWriter writer = null;
        System.out.print("> ");
        boolean redirection = false;
        while (s.hasNext()) {
            String command = s.nextLine();
            if (command.contains(">")) {
                redirection = true;
                String filename = command.split(">")[1].trim();
                File file = new File(filename);
                if (!file.exists()) {
                    file.createNewFile();
                }
                writer = new FileWriter(file, true);
            }
            ProcessBuilder pb = new ProcessBuilder(command.split(" "));
            Process p = pb.start();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                if (!redirection) {
                    System.out.println(line);
                } else {
                    writer.write(line);
                    writer.flush();
                }
            }
            if (writer != null)
                writer.close();
            redirection = false;
            System.out.print("> ");
        }
        s.close();
    }
}

