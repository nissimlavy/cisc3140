import java.util.Scanner;

public class HW1Q5 {

    public static void main(String[] args) {
        Scanner scanIn = new Scanner(System.in);
        while (scanIn.hasNext()) {
            String coords = scanIn.nextLine();
            String[] split = coords.split(" ");
            if (split.length != 3) {
                System.out.println("Please enter 3 coordinates separated by spaces.");
            } else {
                double distance = Math.sqrt(Math.pow(
                        Double.parseDouble(split[0]) - Double.parseDouble(split[1]), 2)
                        + Math.pow(Double.parseDouble(split[1]) - Double.parseDouble(split[2]), 2));
                System.out.println("Distance : " + distance);
                double side1 = Double.parseDouble(split[0]);
                double side2 = Double.parseDouble(split[1]);
                double side3 = Double.parseDouble(split[2]);
                if (side1 + side2 > side3 && side2 + side3 > side1 & side1 + side3 > side2) {
                    System.out.println("real triangle");
                } else {
                    System.out.println("not a real triangle");
                }
            }

        }
        scanIn.close();
    }

}
