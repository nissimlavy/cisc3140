import java.util.Random;

public class HW1Q2 {
    public static void main(String[] args) {
        Random rand = new Random();
        String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept",
                "Oct", "Nov", "Dec" };

        for (int i = 0; i < 12; i++) {
            int randomNum = rand.nextInt((11 - 0) + 0) + 0;
            System.out.println(months[randomNum]);
        }
    }

}
