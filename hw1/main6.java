import java.util.Scanner;

// Referenced http://stackoverflow.com/questions/22032342/java-two-circles-overlap-or-inside for algorithm.

public class HW1Q6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out
                .print("Enter first circle's center coordinates and radius -- center (x, y) coordinates, and radius: ");
        while (input.hasNext()) {
            Circle circle1 = new Circle();
            Circle circle2 = new Circle();

            circle1.setX(input.nextDouble());
            circle1.setY(input.nextDouble());
            circle1.setRadius(input.nextDouble());
            System.out
                    .print("Enter second circle's center coordinates and radius -- center (x, y) coordinates, and radius: ");
            circle2.setX(input.nextDouble());
            circle2.setY(input.nextDouble());
            circle2.setRadius(input.nextDouble());
            double distance = Math.pow((circle1.getX() - circle2.getX())
                    * (circle1.getX() - circle2.getX()) + (circle1.getY() - circle2.getY())
                    * (circle1.getY() - circle2.getY()), 0.5);
            if (circle2.getRadius() >= circle1.getRadius()
                    && distance <= (circle2.getRadius() - circle1.getRadius())) {
                System.out.println("Circle 1 is inside Circle 2");
            } else if (circle1.getRadius() >= circle2.getRadius()
                    && distance <= (circle1.getRadius() - circle2.getRadius())) {
                System.out.println("Circle 2 is inside Circle 1");
            } else if (distance > (circle1.getRadius() + circle2.getRadius())) {
                System.out.println("Circle 2 and circle 2 do not overlap");
            } else {
                System.out.println("Circle 1 and cirlce 2 do overlap");
            }
            System.out
                    .print("Enter first circle's center coordinates and radius -- center (x, y) coordinates, and radius: ");
        }
        input.close();
    }
}

class Circle {
    private double x;
    private double y;
    private double radius;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
