import java.util.Arrays;

public class MyStringDriver {
    public static void main(String[] args) {
        char[] charr = { 'j', 'a', 'v', 'a' };
        MyString myString = new MyString(charr);
        System.out.println(myString.getMyString());
        System.out.println(myString.toString());
        System.out.println(myString.length());
        System.out.println(myString.toUppercase());
        System.out.println(myString.toLowercase());
        char[] charr2 = { 'j', 'a', 'v', 'a' };
        MyString anotherString = new MyString(charr2);
        if (myString.equals(anotherString)) {
            System.out.println("Equal");
        } else {
            System.out.println("Not equal");
        }
        char[] charr3 = { 'l', 'a', 'v', 'a' };
        MyString aThirdOne = new MyString(charr3);
        if (myString.equals(aThirdOne)) {
            System.out.println("Equal");
        } else {
            System.out.println("Not equal");
        }
        System.out.println(myString.substring(2, 3));
        System.out.println(myString.charAt(2));
        System.out.println(MyString.valueOf(78));
    }
}

final class MyString {
    private final char value[];

    public MyString(char value[]) {
        this.value = Arrays.copyOf(value, value.length);
    }

    public char charAt(int index) {
        return value[index];
    }

    public int length() {
        return value.length;
    }

    public MyString substring(int beginIndex, int endIndex) {
        int subLen = endIndex - beginIndex;
        if (((beginIndex == 0) && (endIndex == value.length))) {
            return this;
        } else {
            char newValue[] = new char[subLen + 1];
            for (int i = beginIndex; i < value.length; i++) {
                newValue[i - beginIndex] = value[i];
            }
            return new MyString(newValue);
        }
    }

    public MyString toLowercase() {
        char[] newValue = new char[this.value.length];
        for (int i = 0; i < this.value.length; i++) {
            newValue[i] = Character.toLowerCase(this.value[i]);
        }
        return new MyString(newValue);
    }

    public MyString toUppercase() {
        char[] newValue = new char[this.value.length];
        for (int i = 0; i < this.value.length; i++) {
            newValue[i] = Character.toUpperCase(this.value[i]);
        }
        return new MyString(newValue);
    }

    public boolean equals(Object anObject) {
        if (this == anObject) {
            return true;
        }
        if (anObject instanceof MyString) {
            MyString anotherString = (MyString) anObject;
            int n = value.length;
            if (n == anotherString.value.length) {
                char v1[] = value;
                char v2[] = anotherString.value;
                int i = 0;
                while (n-- != 0) {
                    if (v1[i] != v2[i])
                        return false;
                    i++;
                }
                return true;
            }
        }
        return false;
    }

    public MyString getMyString() {
        return this;
    }

    public String toString() {
        return String.valueOf(this.value);
    }

    public static MyString valueOf(int i) {
        char[] val = { (char) i };
        return new MyString(val);
    }
}
