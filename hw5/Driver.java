package stuff;

public class Driver {

    public static void main(String[] args) {
        Parent p = new Parent();
        System.out.println(p.value);

        Child child = new Child();
        child.printParentValue();
        System.out.println(child.value);
    }
}
