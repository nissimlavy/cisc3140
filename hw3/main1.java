public class Queens {

    private static int numOfSolutions = 0;

    // check if queen placement conflicts with other queens
    public static boolean check(int[] queen, int n) {
        for (int i = 0; i < n; i++) {
            if (queen[i] == queen[n])
                return false; // check column
            if ((queen[i] - queen[n]) == (n - i))
                return false; // check diagonal 1
            if ((queen[n] - queen[i]) == (n - i))
                return false; // check diagonal 2
        }
        return true;
    }

    // calculate all permutations using 'back-tracking' @see https://en.wikipedia.org/wiki/Backtracking
    public static void enumerate(int N) {
        int[] a = new int[N];
        backtrack(a, 0);
    }

    public static void backtrack(int[] queen, int n) {
        int N = queen.length;
        if (n == N) {
            numOfSolutions++;
        } else {
            for (int i = 0; i < N; i++) {
                queen[n] = i;
                if (check(queen, n))
                    backtrack(queen, n + 1); // recursive check for each position
            }
        }
    }

    public static void main(String[] args) {
        enumerate(8); // 8 queens on an 8 x 8 board
        System.out.println(numOfSolutions);
    }

}
